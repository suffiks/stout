package proto

import (
	"shared.saison"
	"google/api/annotations.proto"
	"protoc-gen-swagger/options/annotations.proto"
	"google/protobuf/empty.proto"
)


// Registries contains registry methods
service Registries {
	base_path: "/v1/projects/{project_id}/registries"

	// List registries for a project
	rpc List RegistriesListRequest RegistriesListResponse {
		get: ""
	}

	// Create a new registry in a project
	rpc Create RegistriesCreateRequest Registry {
		post: ""
		body: "*"
	}

	// Delete a registry
	rpc Delete DeleteRequest {
		delete: "/{id}"
	}
}

// Registry contains registry details
msg Registry {
	// Id of the registry
	1 id string 
	// ProjectId the registry is part of
	2 project_id string 
	// Name of the registry
	3 name string 
}

// RegistriesListResponse contains filtered registries
msg RegistriesListResponse {
	// Registries
	1 registries []Registry
}

// RegistriesCreateRequest defines a new registry
msg RegistriesCreateRequest {
	// ProjectId to create the registry in
	1 project_id string required
	// Name of the registry
	2 name string required
	// Email for authentication
	3 email string
	// Server to authenticate to
	4 server string required
	// Username for authentication
	5 username string
	// Password for authentication
	6 password string
}

// RegistriesListRequest filters registries for a project
msg RegistriesListRequest {
	// ProjectId to list from
	1 project_id string required
	// Selection
	2 selection ListRequest
}
