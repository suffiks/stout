# Stout
Stout aims to simplify deploying applications to a Kubernetes clusters.

## Project goals

- Deploy applications
- Expose applications to the web
- Sync DNS ([external-dns](https://github.com/kubernetes-incubator/external-dns))
- Create TLS Certificates ([cert-manager](https://github.com/jetstack/cert-manager))
- Capture Prometheus metrics
- Capture OpenTracing traces
- Capture network metrics ([istio](https://istio.io))

## Development
Install minikube or micro-k8s, protoc and docker-compose

- To install build dependencies (not protoc): `make deps`
- To build `.proto` files, run `make gen_protos`
- To run a swagger-ui server, run `make swagger`

Run runtime dependencies using `docker-compose up -d`
