package k8s

import (
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func ServicesCreate(projectID, app string, ports []Port) error {
	svcPorts := []v1.ServicePort{}

	for _, port := range ports {
		svcPorts = append(svcPorts, v1.ServicePort{
			Port:       port.Port,
			TargetPort: intstr.FromString(port.Name),
			Name:       port.Name,
		})
	}

	s := &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:   app,
			Labels: defaultLabels(),
		},
		Spec: v1.ServiceSpec{
			Selector: map[string]string{
				"app": app,
			},
			Ports: svcPorts,
		},
	}

	_, err := clientSet.CoreV1().Services(ns(projectID)).Create(s)
	return err
}

// type ServiceType int

// const (
// 	ServiceTypeInternal ServiceType = iota
// 	ServiceTypeExternal
// )

// type Service struct {
// 	ID        string
// 	ProjectID string
// 	Name      string
// 	App       string
// 	Target    string
// 	Expose    int32
// 	Type      ServiceType
// }

// func (s *Service) v1() *v1.Service {
// 	typ := v1.ServiceTypeClusterIP

// 	annotations := map[string]string{
// 		AnnotationName: s.Name,
// 	}

// 	return &v1.Service{
// 		ObjectMeta: metav1.ObjectMeta{
// 			Name:        s.ID,
// 			Labels:      defaultLabels(),
// 			Annotations: annotations,
// 		},
// 		Spec: v1.ServiceSpec{
// 			Type: typ,
// 			Selector: map[string]string{
// 				"app": s.App,
// 			},
// 			Ports: []v1.ServicePort{
// 				v1.ServicePort{
// 					Port:       s.Expose,
// 					TargetPort: intstr.FromString(s.Target),
// 				},
// 			},
// 		},
// 	}
// }

// func (s *Service) Service() *proto.Service {
// 	typ := proto.ServiceType_internal
// 	if s.Type == ServiceTypeExternal {
// 		typ = proto.ServiceType_external
// 	}
// 	return &proto.Service{
// 		Id:        s.ID,
// 		ProjectId: s.ProjectID,
// 		Name:      s.Name,
// 		Type:      typ,
// 		AppId:     s.App,
// 		PortName:  s.Target,
// 		Expose:    s.Expose,
// 	}
// }
