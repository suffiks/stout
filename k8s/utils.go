package k8s

func unptrInt32(ip *int32) int32 {
	if ip == nil {
		return 0
	}

	return *ip
}
