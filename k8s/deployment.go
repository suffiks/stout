package k8s

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/suffiks/stout/proto"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const RevisionAnnotation = "deployment.kubernetes.io/revision"

type Env struct {
	Name  string
	Value string
}

type Port struct {
	Port int32
	Name string
}

type ReplicaStatus struct {
	Desired int32
	Current int32
	Ready   int32
}

type Revision struct {
	Number        int
	Created       time.Time
	ReplicaStatus ReplicaStatus
}

type Deployment struct {
	Name        string
	Labels      map[string]string
	Replicas    int32
	PullSecret  string
	Annotations map[string]string

	containers []*Container
	status     string
}

func NewDeployment(name string, pullSecret string) (*Deployment, error) {
	if !regexID.MatchString(name) {
		return nil, fmt.Errorf("invalid name")
	}

	return &Deployment{
		Name:        name,
		Labels:      map[string]string{"app": name},
		Annotations: make(map[string]string),
		PullSecret:  pullSecret,
	}, nil
}

func (d *Deployment) v1() *appsv1.Deployment {
	lbls := defaultLabels(d.Labels)
	fls := false

	depl := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:        d.Name,
			Labels:      lbls,
			Annotations: d.Annotations,
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: lbls,
			},
			Replicas: &d.Replicas,
			Template: v1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: lbls,
				},
				Spec: v1.PodSpec{
					AutomountServiceAccountToken: &fls,
				},
			},
		},
	}

	if d.PullSecret != "" {
		depl.Spec.Template.Spec.ImagePullSecrets = []v1.LocalObjectReference{
			v1.LocalObjectReference{
				Name: d.PullSecret,
			},
		}
	}

	for _, c := range d.containers {
		depl.Spec.Template.Spec.Containers = append(depl.Spec.Template.Spec.Containers, c.v1())
	}

	return depl
}

func (d *Deployment) AddContainer(name, image string, cpu, memory int64, ports []Port, env []Env) error {
	c, err := NewContainer(name, image, cpu, memory, ports, env)
	if err != nil {
		return err
	}
	d.containers = append(d.containers, c)
	return nil
}

func (d *Deployment) App() *proto.App {
	return &proto.App{
		Id:     d.Name,
		Name:   d.Annotations[AnnotationName],
		Status: d.status,
	}
}

func (d *Deployment) AppDetails() *proto.AppDetails {
	return &proto.AppDetails{
		Id:     d.Name,
		Name:   d.Annotations[AnnotationName],
		Status: d.status,
	}
}

type Container struct {
	Name      string
	Image     string
	Ports     []Port
	Env       []Env
	Resources v1.ResourceRequirements
}

func NewContainer(name, image string, cpu, memory int64, ports []Port, env []Env) (*Container, error) {
	if !regexID.MatchString(name) {
		return nil, fmt.Errorf("invalid name")
	}

	cpuq := resource.NewMilliQuantity(cpu, resource.DecimalSI)

	memq := resource.NewQuantity(memory, resource.DecimalSI)

	res := v1.ResourceList{
		v1.ResourceCPU:    *cpuq,
		v1.ResourceMemory: *memq,
	}

	return &Container{
		Name:  name,
		Image: image,
		Resources: v1.ResourceRequirements{
			Limits:   res,
			Requests: res,
		},
		Ports: ports,
		Env:   env,
	}, nil
}

func (c *Container) v1() v1.Container {
	con := v1.Container{
		Name:      c.Name,
		Image:     c.Image,
		Resources: c.Resources,
	}

	for _, p := range c.Ports {
		con.Ports = append(con.Ports, v1.ContainerPort{
			ContainerPort: p.Port,
			Name:          p.Name,
		})
	}

	for _, e := range c.Env {
		con.Env = append(con.Env, v1.EnvVar{
			Name:  e.Name,
			Value: e.Value,
		})
	}

	return con
}

func deploymentFromK8s(d *appsv1.Deployment) *Deployment {
	return &Deployment{
		Name:        d.Name,
		Labels:      d.Labels,
		Annotations: d.Annotations,
		Replicas:    unptrInt32(d.Spec.Replicas),
		status:      deploymentStatus(d.Status),
	}
}

func DeploymentsGet(projectID, name string) (*Deployment, error) {
	depl, err := clientSet.AppsV1().Deployments(ns(projectID)).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return deploymentFromK8s(depl), nil
}

func DeploymentsCreate(projectID string, deployment *Deployment) error {
	depl := deployment.v1()

	_, err := clientSet.AppsV1().Deployments(ns(projectID)).Create(depl)
	return err
}

func DeploymentsList(projectID string) ([]*Deployment, error) {
	depls, err := clientSet.AppsV1().Deployments(ns(projectID)).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	var ret []*Deployment
	for _, d := range depls.Items {
		ret = append(ret, deploymentFromK8s(&d))
	}

	return ret, nil
}

func DeploymentsDelete(projectID, name string) error {
	return clientSet.AppsV1().Deployments(ns(projectID)).Delete(name, nil)
}

func DeploymentsRevisions(projectID, name string) error {
	apps := clientSet.AppsV1()
	depl, err := apps.Deployments(ns(projectID)).Get(name, metav1.GetOptions{})
	if err != nil {
		return err
	}

	selector, err := metav1.LabelSelectorAsSelector(depl.Spec.Selector)
	if err != nil {
		return err
	}

	opts := metav1.ListOptions{
		LabelSelector: selector.String(),
	}
	all, err := clientSet.AppsV1().ReplicaSets(ns(projectID)).List(opts)
	if err != nil {
		return err
	}

	owned := make([]*Revision, 0, len(all.Items))
	for _, rs := range all.Items {
		if metav1.IsControlledBy(&rs, depl) {
			i, err := strconv.ParseInt(rs.Annotations[RevisionAnnotation], 0, 32)
			if err != nil {
				i = -1
			}
			owned = append(owned, &Revision{
				Created: rs.GetCreationTimestamp().Time,
				Number:  int(i),
				ReplicaStatus: ReplicaStatus{
					Desired: unptrInt32(rs.Spec.Replicas),
					Current: rs.Status.Replicas,
					Ready:   rs.Status.ReadyReplicas,
				},
			})
		}
	}
	return nil
}

func deploymentStatus(ds appsv1.DeploymentStatus) string {
	switch {
	case ds.UnavailableReplicas > 0:
		return "error"
	case ds.Replicas != ds.ReadyReplicas:
		return "warning"
	default:
		return "ok"
	}
}
