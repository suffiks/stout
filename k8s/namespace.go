package k8s

import (
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// NamespaceCreate creates a new namespace
func NamespaceCreate(projectID string) (*v1.Namespace, error) {
	nso := &v1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   ns(projectID),
			Labels: defaultLabels(),
		},
	}

	return clientSet.Core().Namespaces().Create(nso)
}

// NamespaceDelete deletes a namespace
func NamespaceDelete(projectID string) error {
	return clientSet.Core().Namespaces().Delete(ns(projectID), nil)
}

func defaultLabels(extra ...map[string]string) map[string]string {
	lbs := map[string]string{
		"stout": "true",
	}

	for _, m := range extra {
		for k, v := range m {
			lbs[k] = v
		}
	}

	return lbs
}

func ns(projectID string) string {
	return "stout-" + projectID
}
