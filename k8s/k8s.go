package k8s

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"

	"golang.org/x/oauth2"
	"k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

var clientSet *kubernetes.Clientset
var saFile = "./dev/sa.yaml"

var regexID = regexp.MustCompile(`[a-z]{1}[a-z0-9\-_]{1,14}[a-z0-9]{1}`)

func InitClient() error {
	decode := scheme.Codecs.UniversalDeserializer().Decode

	b, err := ioutil.ReadFile(saFile)
	if err != nil {
		return err
	}

	obj, _, err := decode(b, nil, nil)
	if err != nil {
		return err
	}

	saObj, ok := obj.(*v1.Secret)
	if !ok {
		return fmt.Errorf("%q is invalid SA secret", saFile)
	}

	sa := &serviceAccount{}
	var caData []byte
	for k, v := range saObj.Data {
		switch k {
		case "token":
			sa.token = b64toString(v)
		case "ca.crt":
			caData = v
		}
	}

	config := &rest.Config{
		Host: "http://localhost:8080",
		TLSClientConfig: rest.TLSClientConfig{
			CAData: caData,
		},
		WrapTransport: func(rt http.RoundTripper) http.RoundTripper {
			return &oauth2.Transport{
				Source: sa,
			}
		},
	}

	clientSet, err = kubernetes.NewForConfig(config)
	return err
}

type serviceAccount struct {
	token string
}

func (s *serviceAccount) Token() (*oauth2.Token, error) {
	return &oauth2.Token{
		AccessToken: s.token,
	}, nil
}

func b64toString(b []byte) string {
	var nb = make([]byte, base64.StdEncoding.DecodedLen(len(b)))
	if _, err := base64.StdEncoding.Decode(nb, b); err != nil {
		return ""
	}
	return string(nb)
}
