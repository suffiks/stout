package k8s

import (
	"encoding/json"
	"errors"
	"fmt"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func SecretsCreateRegistry(projectID, name, email, server, username, password string) error {
	cnfJSON := dockerConfigJSON{
		Auths: map[string]dockerConfigEntry{
			server: dockerConfigEntry{
				Username: username,
				Password: password,
				Email:    email,
			},
		},
	}

	b, err := json.Marshal(cnfJSON)
	if err != nil {
		return err
	}

	sec := &v1.Secret{
		Type: v1.SecretTypeDockerConfigJson,
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: defaultLabels(),
		},
		Data: map[string][]byte{
			v1.DockerConfigJsonKey: b,
		},
	}

	_, err = clientSet.Core().Secrets(ns(projectID)).Create(sec)
	return err
}

func SecretsDeleteRegistry(projectID, name string) error {
	return clientSet.Core().Secrets(ns(projectID)).Delete(name, nil)
}

func SecretsRegistryGet(projectID, name string) (*dockerConfigEntry, error) {
	secret, err := clientSet.Core().Secrets(ns(projectID)).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	data, ok := secret.Data[v1.DockerConfigJsonKey]
	if !ok {
		return nil, errors.New("Data not found")
	}

	config := &dockerConfigJSON{}
	if err := json.Unmarshal(data, config); err != nil {
		return nil, err
	}

	for k, v := range config.Auths {
		v.Host = k
		return &v, nil
	}

	return nil, fmt.Errorf("No config found in %q", name)
}

// dockerConfigJSON represents a local docker auth config file
// for pulling images.
type dockerConfigJSON struct {
	Auths dockerConfig `json:"auths"`
	// +optional
	HttpHeaders map[string]string `json:"HttpHeaders,omitempty"`
}

// dockerConfig represents the config file used by the docker CLI.
// This config that represents the credentials that should be used
// when pulling images from specific image repositories.
type dockerConfig map[string]dockerConfigEntry

type dockerConfigEntry struct {
	Host     string `json:"-"`
	Username string
	Password string
	Email    string
}
