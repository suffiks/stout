package k8s

import (
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const resourceQuotaName = "quota"

func ResourceQuotaCreate(projectID string) (*v1.ResourceQuota, error) {
	rq := &v1.ResourceQuota{
		ObjectMeta: metav1.ObjectMeta{
			Name:   resourceQuotaName,
			Labels: defaultLabels(),
		},
		Spec: v1.ResourceQuotaSpec{
			Hard: v1.ResourceList{
				v1.ResourceLimitsCPU:              resource.MustParse("1"),
				v1.ResourceLimitsMemory:           resource.MustParse("1G"),
				v1.ResourcePersistentVolumeClaims: resource.MustParse("1"),
				v1.ResourceRequestsStorage:        resource.MustParse("256M"),
			},
		},
	}

	return clientSet.Core().ResourceQuotas(ns(projectID)).Create(rq)
}

func ResourceQuotaGet(projectID string) (*v1.ResourceQuota, error) {
	return clientSet.Core().ResourceQuotas(ns(projectID)).Get(resourceQuotaName, metav1.GetOptions{})
}
