package docker

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/heroku/docker-registry-client/registry"
)

type Auth struct {
	Username string
	Password string
}

func (a *Auth) username() string {
	if a == nil {
		return ""
	}
	return a.Username
}

func (a *Auth) password() string {
	if a == nil {
		return ""
	}
	return a.Password
}

type ManifestDigestConfig struct {
	Architecture string
	OS           string `json:"os"`
	Config       ManifestConfig
}

type ManifestConfig struct {
	Env          []string
	ExposedPorts map[string]struct{}
	Volumes      map[string]struct{}
}

func DownloadManifestConfig(regURL, name, tag string, auth *Auth) (*ManifestDigestConfig, error) {
	url := strings.TrimSuffix(regURL, "/")
	hub := registry.Registry{
		Client: &http.Client{
			Timeout:   time.Second * 10,
			Transport: registry.WrapTransport(http.DefaultTransport, url, auth.username(), auth.password()),
		},
		Logf: registry.Quiet,
		URL:  url,
	}

	if err := hub.Ping(); err != nil {
		return nil, err
	}

	tags, err := hub.ManifestV2(name, tag)
	if err != nil {
		return nil, err
	}

	rc, err := hub.DownloadBlob(name, tags.Config.Digest)
	if err != nil {
		return nil, err
	}
	defer rc.Close()

	config := &ManifestDigestConfig{}
	return config, json.NewDecoder(rc).Decode(config)
}
