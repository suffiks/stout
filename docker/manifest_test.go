package docker

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestManifest(t *testing.T) {
	url := "https://registry-1.docker.io/"
	// auth := &Auth{
	// 	Username: "",
	// 	Password: "",
	// }
	x, err := DownloadManifestConfig(url, "thokra/landing", "latest", nil)
	if err != nil {
		t.Fatal(err)
	}

	spew.Dump(x)
}
