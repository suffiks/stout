gen_protos:
	saison build -c -o saison/protofiles saison
	protoc \
		-I ${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/ \
		-I ${GOPATH}/src/github.com/gogo/protobuf/protobuf/ \
		-I $(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		-I saison/protofiles/ \
		saison/protofiles/*.proto \
		--gofast_out=\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
plugins=grpc:proto \
		--grpc-gateway_out=logtostderr=true:./proto \
		--swagger_out=logtostderr=true,allow_merge=true:./dev \

	go install gitlab.com/suffiks/stout/proto

deps:
	go get -u github.com/gogo/protobuf/protoc-gen-gofast \
		 github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
		 github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway \
		 git.d09.no/thokra/saison

swagger:
	docker run --rm -ti -p 4000:8080 \
		-v $(shell pwd)/dev:/app \
		-e SWAGGER_JSON=/app/apidocs.swagger.json \
		swaggerapi/swagger-ui
