#!/bin/bash
set -xe

kubectl create serviceaccount stout

kubectl create clusterrolebinding stout-cluster-admin-binding \
    --clusterrole cluster-admin \
    --user stout

kubectl get secret $(kubectl get secret | grep stout-token | awk '{print $1}') -o yaml > dev/sa.yaml
