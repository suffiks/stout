package rethink

import (
	"gitlab.com/suffiks/stout/storage"
	"github.com/rs/xid"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
	rt "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// Registries contains methods for working on registries
type Registries struct {
	Conn r.QueryExecutor
}

func (r *Registries) table() r.Term {
	return rt.Table("registries")
}

// Get a registry by id
func (r *Registries) Get(id string) (*storage.Registry, error) {
	c, err := r.table().Get(id).Run(r.Conn)
	if err != nil {
		return nil, err
	}

	reg := &storage.Registry{}
	return reg, c.One(reg)
}

// ForProject returns registries for a given project
func (r *Registries) ForProject(projectID string, offset, limit int) (ret []*storage.Registry, err error) {
	offset, limit = storage.DefaultOffsetLimit(offset, limit)

	c, err := r.table().GetAllByIndex("project_id", projectID).OrderBy("id").Limit(limit).Skip(offset).Run(r.Conn)
	if err != nil {
		return nil, err
	}

	return ret, c.All(&ret)
}

// Save a registry
func (r *Registries) Save(reg *storage.Registry) error {
	if reg.ID == "" {
		reg.ID = r.GenID()
	}
	_, err := r.table().Insert(reg).RunWrite(r.Conn)
	if err != nil {
		return err
	}

	return nil
}

// Delete a registry
func (r *Registries) Delete(reg *storage.Registry) error {
	_, err := r.table().Get(reg.ID).Delete().RunWrite(r.Conn)
	return err
}

// GenID retuns a new ID
func (r *Registries) GenID() string {
	return xid.New().String()
}
