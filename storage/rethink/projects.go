package rethink

import (
	"gitlab.com/suffiks/stout/storage"
	"github.com/rs/xid"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

/*
Create index for projects containing users
r.db("stout").table("projects").indexCreate("users", {multi: true})
*/

// Projects contains methods for working on projects
type Projects struct {
	Conn r.QueryExecutor
}

func (p *Projects) table() r.Term {
	return r.Table("projects")
}

// Get a project by id
func (p *Projects) Get(userID, id string) (*storage.Project, error) {
	c, err := p.table().GetAllByIndex("users", userID).Filter(r.Row.Field("id").Eq(id)).Run(p.Conn)
	if err != nil {
		return nil, err
	}

	proj := &storage.Project{}
	return proj, c.One(proj)
}

// ForUser returns all projects associated with a user
func (p *Projects) ForUser(userID string, offset, limit int) (ret []*storage.Project, err error) {
	offset, limit = storage.DefaultOffsetLimit(offset, limit)

	c, err := p.table().GetAllByIndex("users", userID).OrderBy("id").Limit(limit).Skip(offset).Run(p.Conn)
	if err != nil {
		return nil, err
	}

	return ret, c.All(&ret)
}

// Save a project
func (p *Projects) Save(proj *storage.Project) error {
	if proj.ID == "" {
		proj.ID = xid.New().String()
	}
	_, err := p.table().Insert(proj, r.InsertOpts{Conflict: "update"}).RunWrite(p.Conn)
	if err != nil {
		return err
	}

	return nil
}

// Delete a project
func (p *Projects) Delete(proj *storage.Project) error {
	_, err := p.table().Get(proj.ID).Delete().RunWrite(p.Conn)
	return err
}

// GenID generates a new ID
func (p *Projects) GenID() string {
	return xid.New().String()
}
