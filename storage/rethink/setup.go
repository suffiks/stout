package rethink

import (
	"gitlab.com/suffiks/stout/storage"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

func Setup() error {
	sess, err := r.Connect(r.ConnectOpts{
		Database: "stout",
	})
	r.SetTags("rethinkdb", "json")
	if err != nil {
		return err
	}

	storage.UsersDB = &Users{Conn: sess}
	storage.ProjectsDB = &Projects{Conn: sess}
	storage.RegistriesDB = &Registries{Conn: sess}

	return nil
}
