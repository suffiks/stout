package rethink

import (
	"gitlab.com/suffiks/stout/storage"
	"github.com/rs/xid"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// Users contains methods for working on users
type Users struct {
	Conn r.QueryExecutor
}

func (u *Users) table() r.Term {
	return r.Table("users")
}

// ByEmail finds a user by it's email
func (u *Users) ByEmail(email string) (*storage.User, error) {
	c, err := u.table().GetAllByIndex("email", email).Run(u.Conn)
	if err != nil {
		return nil, err
	}
	usr := &storage.User{}
	return usr, c.One(&usr)
}

// Save a user
func (u *Users) Save(usr *storage.User) error {
	if usr.ID == "" {
		usr.ID = xid.New().String()
	}
	_, err := u.table().Insert(usr).RunWrite(u.Conn)
	if err != nil {
		return err
	}

	return nil
}

// ByIDs returns a list of users by id
func (u *Users) ByIDs(ids []string) (ret []*storage.User, err error) {
	iids := []interface{}{}
	for _, i := range ids {
		iids = append(iids, i)
	}

	c, err := u.table().GetAll(iids...).Run(u.Conn)
	if err != nil {
		return nil, err
	}

	return ret, c.All(&ret)
}
