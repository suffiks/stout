package storage

import (
	"errors"
)

var ErrInvalidObject = errors.New("invalid object")
