package storage

import (
	"gitlab.com/suffiks/stout/proto"
)

// Project is a project ....
type Project struct {
	ID    string   `json:"id,omitempty"`
	Name  string   `json:"name,omitempty"`
	Users []string `json:"users,omitempty"`
}

// Valid returns true if name is more than 3 characters long
func (p *Project) Valid() bool {
	return len(p.Name) > 3
}

func (p *Project) Proto() *proto.Project {
	return &proto.Project{
		Id:   p.ID,
		Name: p.Name,
	}
}

// Projects contains data methods
type Projects interface {
	Get(userId, id string) (*Project, error)
	ForUser(userID string, offset, limit int) ([]*Project, error)
	Save(*Project) error
	Delete(*Project) error

	GenID() string
}
