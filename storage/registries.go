package storage

import (
	"gitlab.com/suffiks/stout/proto"
)

// Registry is a registry ....
type Registry struct {
	ID        string `json:"id,omitempty"`
	ProjectID string `json:"project_id,omitempty"`
	Name      string `json:"name,omitempty"`
}

// Valid returns true if name is more than 3 characters long
func (r *Registry) Valid() bool {
	switch {
	case len(r.Name) < 3,
		r.ProjectID == "":
		return false
	}

	return true
}

func (r *Registry) Proto() *proto.Registry {
	return &proto.Registry{
		Id:   r.ID,
		Name: r.Name,
	}
}

// Registries contains data methods
type Registries interface {
	ForProject(projectID string, offset, limit int) ([]*Registry, error)
	Get(id string) (*Registry, error)
	Save(*Registry) error
	Delete(*Registry) error

	GenID() string
}
