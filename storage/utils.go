package storage

func DefaultOffsetLimit(offset, limit int) (int, int) {
	if offset <= 0 {
		offset = 0
	}
	if limit <= 0 {
		limit = 20
	}
	return offset, limit
}

var (
	UsersDB      Users
	RegistriesDB Registries
	ProjectsDB   Projects
)
