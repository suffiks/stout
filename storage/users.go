package storage

import (
	"regexp"
	"time"

	"gitlab.com/suffiks/stout/proto"
)

type User struct {
	ID           string    `json:"id,omitempty"`
	TOSAccepted  time.Time `json:"tos_accepted,omitempty"`
	Name         string    `json:"name,omitempty"`
	Email        string    `json:"email,omitempty"`
	PasswordHash []byte    `json:"-" rethinkdb:"password_hash,omitempty"`
}

var emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func (u *User) Valid() bool {
	if u.TOSAccepted.IsZero() {
		return false
	}
	return emailRegexp.MatchString(u.Email)
}

func (u *User) Proto() *proto.User {
	return &proto.User{
		Id:   u.ID,
		Name: u.Name,
	}
}

type Users interface {
	ByIDs(ids []string) ([]*User, error)
	ByEmail(email string) (*User, error)
	Save(*User) error
}
