# Intentions

## Why are we building Stout?

There is a lot of great software built upon Kubernetes. But having to understand interactions between
the systems and Kubernetes itselves is a challenging problem.

With Stout we strive to make deploying applications in a Kubernetes cluster easier.

We elected to not use default terminology used in Kubernetes, to make Stout easier to understand.

## Terminology
When describing an object in Stout we will usually just use the name of the object (e.g. App).

For Kubernetes object we will always prepend `Kubernetes `. (e.g. Kubernetes Deployment)

- **Project** contains applications and users
- **App** describes an application and is exposed to all other apps in the project

## Mapping to Kubernetes

When you create a new project a new Kubernetes Namespace is created. Each project has specified Kubernetes
resource limits which is shared by all objects in the project.

When you create an App, we will create a Kubernetes deployment and a matching Kubernetes service. The
Kubernetes service exposes the ports exposed by the App.

An App can contain environment variables, and we store them directly onto the Kubernetes Deployment
object. We are thinking about sharing configuration across Apps using Kubernetes ConfigMap.
