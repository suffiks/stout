---
weight: 0
---

# Saison
Saison is a small language design to combine [grpc](https://grpc.io), [grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway) and the grpc-gateway Swagger support.

## Define a service

Services are defined using the keyword `service`:

```proto
// Apps contains application methods
service Apps {
	base_path: "/v1/projects/{project_id}/apps"

	// List applications matching provided filter
	rpc List AppsListRequest AppsList {
		get: ""
	}

	// Create application
	rpc Create AppsCreateRequest {
		post: ""
		body: "*"
	}

	// Delete an application
	rpc Delete DeleteRequest {
		delete: "/{id}"
	}
}

```

Comments before a type is used as documentation in the swagger doc.

`base_path` sets the base path for all methods in this service.

RPC calls are defined with `rpc Name Input Output` where `Output` can be empty. It will then use [`google.protobuf.Empty`](https://github.com/protocolbuffers/protobuf/blob/master/src/google/protobuf/empty.proto) as return type.

Options supported inside a RPC declaration:
- HTTP verbs followed by the path to the method
- `body` which you can use to map fields from body to the message
- `security`

## Define a message

You can define a message using the `msg` keyword.

```proto
// AppsCreateRequest contains information for creating a new application
msg AppsCreateRequest {
	// ProjectId is to which project we should add the application
	1 project_id string required
	// Id of the application
	2 id string required
	// Name of the application
	3 name string required
	// Image for the container
	4 image string required
	// Registry is a reference to a secret used to authenticate when pulling the image
	5 registry string
	// Ports to expose
	6 ports []Port
	// Ports to expose
	7 env []Env
	// Cpu limits
	8 cpu int64
	// Memory limits
	9 memory int64
	// Replicas to create
	10 replicas int32
	// AutoDeployment is set if the user want's to enable automatic deployment
	11 auto_deployment AutoDeployment
}
```

Structure for a field is:  `Index Name Type [required]`.

- `Index` should be unique and never change.
- `Name` of the field
- `Type` of the field. Repeated fields have the type prefixed with `[]`
- `required` can optionally be set on the field to mark it as required in Swagger

## Define enums
You can define enums using the `enum` keyword
```proto
// AutoDeployment_Policy
enum Policy {
	// Off
	0 off
	// AllSemver matches all semver tags, including release candidates etc.
	1 all_semver
	// Major matches all version changes
	2 major
	// Minor matches patch and minor changes
	3 minor
	// Patch matches only patch changes
	4 patch
	// TagsAll matches every new tag
	5 tags_all
	// TagsMatch matches the tag defined in the image
	6 tags_match
	// Glob matches a tag matching the glob. Requires PolicyData to be set
	7 glob
}
```

Structure of an enum value is: `Index Name` where the first element must have index `0`.
