---
title: Frontpage
type: docs
---

![Stout](/stout/img/logo.svg)

Stout aims to simplify deploying applications to a Kubernetes clusters.

## Project goals
- Deploy applications
- Expose applications to the web
- Sync DNS ([external-dns](https://github.com/kubernetes-incubator/external-dns))
- Create TLS Certificates ([cert-manager](https://github.com/jetstack/cert-manager))
- Capture Prometheus metrics
- Capture OpenTracing traces
- Capture network metrics ([istio](https://istio.io))
