package stout

import (
	"io"
	"os"

	"github.com/davecgh/go-spew/spew"
	"github.com/heroku/docker-registry-client/registry"
)

func Ports(img string) {
	hub, err := registry.New("https://registry-1.docker.io/", "", "")
	if err != nil {
		panic(err)
	}

	manifest, err := hub.ManifestV2("willnorris/imageproxy", "latest")
	if err != nil {
		panic(err)
	}

	f, err := hub.DownloadBlob("willnorris/imageproxy", manifest.Config.Digest)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	io.Copy(os.Stdout, f)
	spew.Dump(manifest.Config)

}
