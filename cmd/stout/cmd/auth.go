package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/suffiks/stout/proto"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

// authCmd represents the Auth command
func authCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use: "auth",
	}

	fs := cmd.PersistentFlags()

	// fs.StringP("request-file", "f", "", "JSON-file to create request from. '-' to use stdin")
	fs.StringP("server", "s", "localhost:8080", "hostname to connect to")
	fs.BoolP("print-sample-request", "p", false, "print sample request file and exit")
	fs.StringP("response-format", "o", "json", "response format (json or prettyjson)")
	fs.Duration("timeout", 10*time.Second, "client connection timeout")
	fs.Bool("tls", false, "connects to the server with TLS")
	// fs.String("tls-server-name", "localhost", "tls server name override")
	// fs.Bool("tls-insecure-skip-verify", false, "INSECURE: skip tls checks")
	// fs.String("tls-ca-cert-file", "", "ca certificate file")
	// fs.String("tls-cert-file", "", "client certificate file")
	// fs.String("tls-key-file", "", "client key file")
	// fs.String("auth-token", "", "authorization token")
	// fs.String("auth-token-type", "Bearer", "authorization token type")
	// fs.String("jwt-key", "", "jwt key")
	// fs.String("jwt-key-file", "", "jwt key file")
	return cmd
}

func authRegisterCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:  "register",
		Long: "Set client\n\nYou can use environment variables with the same name of the command flags.\nAll caps and s/-/_, e.g. SERVER_ADDR.",
		Example: `
Save a sample request to a file (or refer to your protobuf descriptor to create one):
	set -p > req.json
Submit request using file:
	set -f req.json
Authenticate using the Authorization header (requires transport security):
	export AUTH_TOKEN=your_access_token
	export SERVER_ADDR=api.example.com:443
	echo '{json}' | set --tls`,
		Run: func(cmd *cobra.Command, args []string) {
			conn, err := getConn(cmd.Flags())
			handleError(err)
			client := proto.NewAuthClient(conn)

			in := &proto.RegistrationRequest{}
			resp, err := client.Register(context.Background(), in)
			handleError(err)

			enc := json.NewEncoder(os.Stdout)
			if viper.GetString("response-format") == "prettyjson" {
				enc.SetIndent("", "  ")
			}
			handleError(enc.Encode(resp))
		},
	}

	return cmd
}

func handleError(err error) {
	if err == nil {
		return
	}
	msg := err.Error()
	fmt.Fprint(os.Stderr, msg)
	if !strings.HasSuffix(msg, "\n") {
		fmt.Fprintln(os.Stderr)
	}
	os.Exit(1)
}

func getConn(flags *pflag.FlagSet) (*grpc.ClientConn, error) {
	opts := []grpc.DialOption{
		grpc.WithTimeout(viper.GetDuration("timeout")),
	}
	if viper.GetBool("tls") {

	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	server, _ := flags.GetString("server")
	fmt.Println("connecting to", server)
	return grpc.Dial(server, opts...)
}

func init() {
	auth := authCmd()
	reg := authRegisterCmd()
	auth.AddCommand(reg)
	RootCmd.AddCommand(auth)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// AuthCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// AuthCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
