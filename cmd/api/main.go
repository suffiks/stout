package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strings"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/suffiks/stout/k8s"
	"gitlab.com/suffiks/stout/server"
	"gitlab.com/suffiks/stout/storage/rethink"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
)

var (
	gwport   = 3000
	grpcPort = 3001
)

func main() {
	g, _ := errgroup.WithContext(context.Background())

	g.Go(serveGrpc)
	g.Go(serveGW)

	if err := k8s.InitClient(); err != nil {
		log.Println(err)
	}

	if err := g.Wait(); err != nil {
		log.Fatal(err)
	}
}

func serveGrpc() error {
	if err := rethink.Setup(); err != nil {
		return err
	}

	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_opentracing.StreamServerInterceptor(),
			grpc_prometheus.StreamServerInterceptor,
			grpc_auth.StreamServerInterceptor(server.AuthFunc),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_opentracing.UnaryServerInterceptor(),
			grpc_prometheus.UnaryServerInterceptor,
			grpc_auth.UnaryServerInterceptor(server.AuthFunc),
		)),
	)

	server.Register(grpcServer)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", grpcPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	fmt.Printf("GRPC listening on :%d\n", grpcPort)
	return grpcServer.Serve(lis)
}

func serveGW() error {
	gwmux := runtime.NewServeMux()
	dopts := []grpc.DialOption{grpc.WithInsecure()}
	ctx := context.Background()

	if err := server.RegisterGateways(ctx, gwmux, fmt.Sprintf("localhost:%d", grpcPort), dopts); err != nil {
		return err
	}

	mux := http.NewServeMux()

	mux.HandleFunc("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		f, err := os.OpenFile("dev/apidocs.swagger.json", os.O_RDONLY, 0644)
		if err != nil {
			panic(err)
		}

		stat, _ := f.Stat()
		http.ServeContent(w, r, "swagger.json", stat.ModTime(), f)
	})
	mux.Handle("/", gwmux)

	fmt.Printf("Rest gateway listening on :%d\n", gwport)
	err := http.ListenAndServe(fmt.Sprintf(":%d", gwport), allowCORS(mux))
	if err != nil {
		log.Println(err)
	}
	return err
}

func allowCORS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if origin := r.Header.Get("Origin"); origin != "" {
			w.Header().Set("Access-Control-Allow-Origin", origin)
			if r.Method == "OPTIONS" && r.Header.Get("Access-Control-Request-Method") != "" {
				preflightHandler(w, r)
				return
			}
		}
		h.ServeHTTP(w, r)
	})
}

// preflightHandler adds the necessary headers in order to serve
// CORS from any origin using the methods "GET", "HEAD", "POST", "PUT", "DELETE"
// We insist, don't do this without consideration in production systems.
func preflightHandler(w http.ResponseWriter, r *http.Request) {
	headers := []string{"Content-Type", "Accept", "Authorization"}
	w.Header().Set("Access-Control-Allow-Headers", strings.Join(headers, ","))
	methods := []string{"GET", "HEAD", "POST", "PUT", "DELETE"}
	w.Header().Set("Access-Control-Allow-Methods", strings.Join(methods, ","))
}
