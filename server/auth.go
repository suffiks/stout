package server

import (
	"context"
	"time"

	"gitlab.com/suffiks/stout/proto"
	"gitlab.com/suffiks/stout/storage"
	"github.com/dgrijalva/jwt-go"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// AuthFunc implements GRPC Authentication
func AuthFunc(ctx context.Context) (context.Context, error) {
	token, err := grpc_auth.AuthFromMD(ctx, "bearer")
	if err != nil {
		return ctx, err
	}

	sub, err := tokenValid(token)
	if err != nil {
		return ctx, status.Error(codes.Unauthenticated, err.Error())
	}

	ctx = context.WithValue(ctx, userID, sub)
	return ctx, nil
}

type auth struct {
	users storage.Users
}

func init() {
	register(proto.RegisterAuthHandlerFromEndpoint, func(s *grpc.Server) {
		proto.RegisterAuthServer(s, NewAuth(storage.UsersDB))
	})
}

// NewAuth returns a new Auth server
func NewAuth(users storage.Users) *auth {
	return &auth{
		users: users,
	}
}

func (a *auth) Register(ctx context.Context, req *proto.RegistrationRequest) (*proto.UserAuthResponse, error) {
	user := &storage.User{
		Email: req.Email,
		Name:  req.Name,
	}
	if req.Tos {
		user.TOSAccepted = time.Now()
	}

	if !user.Valid() {
		return nil, status.Error(codes.InvalidArgument, "invalid user object")
	}

	echeck, err := a.users.ByEmail(user.Email)
	if err == nil && echeck != nil {
		return nil, status.Error(codes.AlreadyExists, "email is taken")
	}

	user.PasswordHash, err = bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}

	if err := a.users.Save(user); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	token, err := a.createToken(user)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.UserAuthResponse{
		Token: token,
	}, nil
}

func (a *auth) Login(ctx context.Context, req *proto.LoginRequest) (*proto.UserAuthResponse, error) {
	usr, err := a.users.ByEmail(req.Email)
	if err != nil || usr == nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	if err = bcrypt.CompareHashAndPassword(usr.PasswordHash, []byte(req.Password)); err != nil {
		return nil, status.Error(codes.Unauthenticated, err.Error())
	}

	token, err := a.createToken(usr)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.UserAuthResponse{
		Token: token,
	}, nil
}

// AuthFuncOverride overrides the grpc auth to allow all requests
func (a *auth) AuthFuncOverride(ctx context.Context, fullMethodName string) (context.Context, error) {
	return ctx, nil
}

func (a *auth) createToken(usr *storage.User) (string, error) {
	claims := &jwtClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(24 * time.Hour).Unix(),
			IssuedAt:  time.Now().Unix(),
			Issuer:    "stout",
			Subject:   usr.ID,
		},
		Name: usr.Name,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signingKey := []byte("averylongstring")
	return token.SignedString(signingKey)
}

func tokenValid(tokenString string) (sub string, err error) {
	claims := &jwtClaims{}

	_, err = jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) {
		if t.Method != jwt.SigningMethodHS256 {
			return nil, status.Error(codes.FailedPrecondition, "unexpected token signature")
		}
		return []byte("averylongstring"), nil
	})

	return claims.Subject, err
}

type jwtClaims struct {
	jwt.StandardClaims
	Name string `json:"name"`
}
