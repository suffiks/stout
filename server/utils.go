package server

import (
	"context"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
)

type ctxType int

var userID ctxType = 1

func ctxUserID(ctx context.Context) string {
	v := ctx.Value(userID)
	if v == nil {
		return ""
	}

	return v.(string)
}

type gatewayFunc func(context.Context, *runtime.ServeMux, string, []grpc.DialOption) error
type serverFunc func(*grpc.Server)

var servers = []serverFunc{}
var gateways = []gatewayFunc{}

func register(gwFunc gatewayFunc, sFunc serverFunc) {
	servers = append(servers, sFunc)
	gateways = append(gateways, gwFunc)
}

func Register(server *grpc.Server) {
	for _, sf := range servers {
		sf(server)
	}
}

func RegisterGateways(ctx context.Context, mux *runtime.ServeMux, endpoint string, opts []grpc.DialOption) error {
	for _, gf := range gateways {
		if err := gf(ctx, mux, endpoint, opts); err != nil {
			return err
		}
	}

	return nil
}
