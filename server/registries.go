package server

import (
	"context"

	"gitlab.com/suffiks/stout/k8s"
	"gitlab.com/suffiks/stout/proto"
	"gitlab.com/suffiks/stout/storage"
	"github.com/gogo/protobuf/types"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func init() {
	register(proto.RegisterRegistriesHandlerFromEndpoint, func(s *grpc.Server) {
		proto.RegisterRegistriesServer(s, NewRegistries(storage.RegistriesDB))
	})
}

// Registries implements the proto server
type Registries struct {
	storage storage.Registries
}

var _ proto.RegistriesServer = &Registries{}

// NewRegistries returns a new Registries server
func NewRegistries(storage storage.Registries) *Registries {
	return &Registries{
		storage: storage,
	}
}

// Create a new registry
func (r *Registries) Create(ctx context.Context, req *proto.RegistriesCreateRequest) (*proto.Registry, error) {
	reg := &storage.Registry{
		ID:        r.storage.GenID(),
		ProjectID: req.ProjectId,
		Name:      req.Name,
	}

	if !reg.Valid() {
		return nil, status.Error(codes.InvalidArgument, "invalid registry")
	}

	if err := k8s.SecretsCreateRegistry(reg.ProjectID, reg.ID, req.Email, req.Server, req.Username, req.Password); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := r.storage.Save(reg); err != nil {
		_ = k8s.SecretsDeleteRegistry(reg.ProjectID, reg.ID)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return reg.Proto(), nil
}

func (r *Registries) List(ctx context.Context, req *proto.RegistriesListRequest) (*proto.RegistriesListResponse, error) {
	regs, err := r.storage.ForProject(req.ProjectId, int(req.GetSelection().GetOffset()), int(req.GetSelection().GetLimit()))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp := &proto.RegistriesListResponse{}
	for _, reg := range regs {
		resp.Registries = append(resp.Registries, reg.Proto())
	}
	return resp, nil
}

func (r *Registries) Delete(ctx context.Context, req *proto.DeleteRequest) (*types.Empty, error) {
	reg, err := r.storage.Get(req.Id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if reg == nil {
		return nil, status.Error(codes.NotFound, "registry not found")
	}

	if err := k8s.SecretsDeleteRegistry(reg.ProjectID, reg.ID); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := r.storage.Delete(reg); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.Empty{}, nil
}
