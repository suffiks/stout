package server

import (
	"context"
	"log"

	"github.com/gogo/protobuf/types"
	"gitlab.com/suffiks/stout/k8s"
	"gitlab.com/suffiks/stout/proto"
	"gitlab.com/suffiks/stout/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func init() {
	register(proto.RegisterAppsHandlerFromEndpoint, func(s *grpc.Server) {
		proto.RegisterAppsServer(s, NewApps(storage.RegistriesDB))
	})
}

// Apps implements the proto server
type Apps struct {
	registries storage.Registries
}

var _ proto.AppsServer = &Apps{}

// NewApps returns a new Registries server
func NewApps(storage storage.Registries) *Apps {
	return &Apps{
		registries: storage,
	}
}

func (a *Apps) List(ctx context.Context, req *proto.AppsListRequest) (*proto.AppsList, error) {
	deployments, err := k8s.DeploymentsList(req.ProjectId)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	ret := &proto.AppsList{}
	for _, d := range deployments {
		ret.Apps = append(ret.Apps, d.App())
	}

	return ret, nil
}

func (a *Apps) Get(ctx context.Context, req *proto.AppRequest) (*proto.AppDetails, error) {
	depl, err := k8s.DeploymentsGet(req.ProjectId, req.Id)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	if err := k8s.DeploymentsRevisions(req.ProjectId, req.Id); err != nil {
		log.Println(err)
	}

	return depl.AppDetails(), nil
}

// Create a new app/deployment
func (a *Apps) Create(ctx context.Context, req *proto.AppsCreateRequest) (*types.Empty, error) {
	if req.Registry != "" {
		_, err := a.registries.Get(req.Registry)
		if err != nil {
			return nil, status.Error(codes.NotFound, "registry not found")
		}
	}

	depl, err := k8s.NewDeployment(req.Id, req.Registry)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	a.applyCreateReq(depl, req)
	a.applyKeel(depl, req.AutoDeployment)

	ports := []k8s.Port{}
	for _, p := range req.Ports {
		ports = append(ports, k8s.Port{
			Port: p.Port,
			Name: p.Name,
		})
	}

	env := []k8s.Env{}
	for _, e := range req.Env {
		env = append(env, k8s.Env{
			Name:  e.Name,
			Value: e.Value,
		})
	}

	if err := depl.AddContainer(req.Id, req.Image, req.Cpu, req.Memory, ports, env); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := k8s.DeploymentsCreate(req.ProjectId, depl); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := k8s.ServicesCreate(req.ProjectId, req.Id, ports); err != nil {
		_ = k8s.DeploymentsDelete(req.ProjectId, req.Id)
		return nil, status.Error(codes.Unknown, err.Error())
	}

	return &types.Empty{}, nil
}

func (a *Apps) Delete(ctx context.Context, req *proto.DeleteRequest) (*types.Empty, error) {
	if err := k8s.DeploymentsDelete(req.ProjectId, req.Id); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.Empty{}, nil
}

func (a *Apps) applyCreateReq(depl *k8s.Deployment, req *proto.AppsCreateRequest) {
	depl.Annotations[k8s.AnnotationName] = req.Name
	depl.Replicas = req.Replicas
}

var keelPolicyMappings = map[proto.AutoDeployment_Policy]string{
	proto.AutoDeployment_all_semver: "all",
	proto.AutoDeployment_major:      "major",
	proto.AutoDeployment_minor:      "minor",
	proto.AutoDeployment_patch:      "patch",
	proto.AutoDeployment_tags_all:   "force",
	proto.AutoDeployment_tags_match: "force",
	proto.AutoDeployment_glob:       "glob",
}

func (a *Apps) applyKeel(depl *k8s.Deployment, ad *proto.AutoDeployment) {
	if ad == nil {
		return
	}

	policy := ""
	matchTag := false
	switch ad.Policy {
	case proto.AutoDeployment_off:
		return
	case proto.AutoDeployment_all_semver:
	case proto.AutoDeployment_tags_match:
		matchTag = true
		fallthrough
	case proto.AutoDeployment_glob:
		policy = "glob:" + ad.PolicyData
	default:
		var ok bool
		policy, ok = keelPolicyMappings[ad.Policy]
		if !ok {
			return
		}
	}

	depl.Labels["keel.sh/policy"] = policy
	if matchTag {
		depl.Labels["keel.sh/match-tag"] = "true"
	}
	if ad.RequireApproval {
		depl.Labels["keel.sh/approvals"] = "1"
	}
	if ad.Pull {
		depl.Labels["keel.sh/trigger"] = "poll"
		depl.Annotations["keel.sh/pollSchedule"] = "@every 1h"
	}
}
