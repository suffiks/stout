package server

import (
	"context"
	"log"

	"github.com/gogo/protobuf/types"
	"gitlab.com/suffiks/stout/proto"
	"google.golang.org/grpc"
)

func init() {
	register(proto.RegisterServicesHandlerFromEndpoint, func(s *grpc.Server) {
		proto.RegisterServicesServer(s, NewService())
	})
}

type Service struct{}

func NewService() *Service {
	return &Service{}
}

func (s *Service) List(ctx context.Context, req *proto.ServicesListRequest) (*proto.ServicesListResponse, error) {
	log.Println("Service.List: not implemented")
	return &proto.ServicesListResponse{}, nil
}

func (s *Service) Create(ctx context.Context, req *proto.ServicesCreateRequest) (*proto.Service, error) {
	log.Println("Service.Delete: not implemented")
	return &proto.Service{}, nil
	// svc, err := s.createK8sService(req)
	// if err != nil {
	// 	return nil, err
	// }

	// if err := k8s.ServicesCreate(svc); err != nil {
	// 	return nil, status.Error(codes.Unknown, err.Error())
	// }

	// return svc.Service(), nil
}

func (s *Service) Delete(ctx context.Context, req *proto.DeleteRequest) (*types.Empty, error) {
	log.Println("Service.Delete: not implemented")
	return &types.Empty{}, nil
}

// func (s *Service) createK8sService(req *proto.ServicesCreateRequest) (*k8s.Service, error) {
// 	typ := k8s.ServiceTypeInternal
// 	if req.Type == proto.ServiceType_external {
// 		typ = k8s.ServiceTypeExternal
// 	}

// 	name := strings.TrimSpace(req.Name)
// 	if len(name) < 3 {
// 		return nil, status.Error(codes.InvalidArgument, "name is to short")
// 	}

// 	if len(req.AppId) < 3 {
// 		return nil, status.Error(codes.InvalidArgument, "not valid app_id")
// 	}

// 	return &k8s.Service{
// 		ID:        xid.New().String(),
// 		ProjectID: req.ProjectId,
// 		Name:      name,
// 		Type:      typ,
// 		App:       req.AppId,
// 		Target:    req.PortName,
// 		Expose:    req.Expose,
// 	}, nil
// }
