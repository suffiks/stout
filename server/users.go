package server

import (
	"context"

	"gitlab.com/suffiks/stout/proto"
	"gitlab.com/suffiks/stout/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func init() {
	register(proto.RegisterUsersHandlerFromEndpoint, func(s *grpc.Server) {
		proto.RegisterUsersServer(s, NewUsers(storage.UsersDB, storage.ProjectsDB))
	})
}

// Users implements the proto server
type Users struct {
	storage  storage.Users
	projects storage.Projects
}

var _ proto.UsersServer = &Users{}

// NewUsers returns a new projects server
func NewUsers(storage storage.Users, projects storage.Projects) *Users {
	return &Users{
		storage:  storage,
		projects: projects,
	}
}

func (u *Users) All(ctx context.Context, req *proto.AllUsersRequest) (*proto.AllUsersResponse, error) {
	proj, err := u.projects.Get(ctxUserID(ctx), req.ProjectId)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	users, err := u.storage.ByIDs(proj.Users)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	res := &proto.AllUsersResponse{}
	for _, usr := range users {
		res.Users = append(res.Users, usr.Proto())
	}
	return res, nil
}

func (u *Users) Get(context.Context, *proto.GetUserRequest) (*proto.User, error) {
	panic("not implemented")
}
