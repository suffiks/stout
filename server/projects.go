package server

import (
	"context"
	"log"

	"github.com/gogo/protobuf/types"
	"gitlab.com/suffiks/stout/k8s"
	"gitlab.com/suffiks/stout/proto"
	"gitlab.com/suffiks/stout/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	v1 "k8s.io/api/core/v1"
)

func init() {
	register(proto.RegisterProjectsHandlerFromEndpoint, func(s *grpc.Server) {
		proto.RegisterProjectsServer(s, NewProjects(storage.ProjectsDB))
	})
}

// Projects implements the proto server
type Projects struct {
	storage storage.Projects
}

var _ proto.ProjectsServer = &Projects{}

// NewProjects returns a new projects server
func NewProjects(storage storage.Projects) *Projects {
	return &Projects{
		storage: storage,
	}
}

func (p *Projects) Get(ctx context.Context, req *proto.ProjectRequest) (*proto.ProjectDetails, error) {
	proj, err := p.storage.Get(ctxUserID(ctx), req.Id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	rq, err := k8s.ResourceQuotaGet(proj.ID)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	ret := &proto.ProjectDetails{
		Id:     proj.ID,
		Name:   proj.Name,
		Limits: parseLimits(rq.Status.Hard),
		Used:   parseLimits(rq.Status.Used),
	}

	return ret, nil
}

// Update project details
func (p *Projects) Update(ctx context.Context, req *proto.ProjectsUpdateRequest) (*proto.Project, error) {
	proj, err := p.storage.Get(ctxUserID(ctx), req.Id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	proj.Users = nil
	proj.Name = req.Name
	if !proj.Valid() {
		return nil, status.Error(codes.InvalidArgument, "invalid project details")
	}

	if err := p.storage.Save(proj); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return proj.Proto(), nil
}

// Create a new project
func (p *Projects) Create(ctx context.Context, req *proto.ProjectsCreateRequest) (*proto.Project, error) {
	proj := &storage.Project{
		ID:    p.storage.GenID(),
		Name:  req.Name,
		Users: []string{ctxUserID(ctx)},
	}

	if !proj.Valid() {
		return nil, status.Error(codes.InvalidArgument, "invalid project")
	}

	if _, err := k8s.NamespaceCreate(proj.ID); err != nil {
		_ = p.storage.Delete(proj)
		return nil, status.Error(codes.Internal, err.Error())
	}

	if _, err := k8s.ResourceQuotaCreate(proj.ID); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := p.storage.Save(proj); err != nil {
		_ = k8s.NamespaceDelete(proj.ID)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return proj.Proto(), nil
}

func (p *Projects) List(ctx context.Context, req *proto.ListRequest) (*proto.ProjectsListResponse, error) {
	projs, err := p.storage.ForUser(ctxUserID(ctx), int(req.Offset), int(req.Limit))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp := &proto.ProjectsListResponse{}
	for _, p := range projs {
		resp.Projects = append(resp.Projects, p.Proto())
	}
	return resp, nil
}

func (p *Projects) Delete(ctx context.Context, req *proto.DeleteRequest) (*types.Empty, error) {
	proj, err := p.storage.Get(ctxUserID(ctx), req.Id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if proj == nil {
		return nil, status.Error(codes.NotFound, "project not found")
	}

	if err := k8s.NamespaceDelete(proj.ID); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := p.storage.Delete(proj); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.Empty{}, nil
}

func parseLimits(list v1.ResourceList) *proto.Limits {
	limit := &proto.Limits{}
	// TODO(thokra): These might overflow an int64. Think about exposing a quantity type
	for k, v := range list {
		switch k {
		case "limits.cpu":
			limit.Cpu = v.MilliValue()
		case "limits.memory":
			limit.Memory = v.MilliValue()
		case "requests.storage":
			limit.Storage = v.MilliValue()
		case "persistentvolumeclaims":
			limit.StorageClaims = v.MilliValue()
		default:
			log.Println("Unknown resource key:", k)
			continue
		}
	}

	return limit
}
