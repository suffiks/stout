package server

import (
	"context"
	"fmt"
	"log"
	"strings"

	"gitlab.com/suffiks/stout/docker"

	"gitlab.com/suffiks/stout/k8s"
	"gitlab.com/suffiks/stout/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func init() {
	register(proto.RegisterDockerHandlerFromEndpoint, func(s *grpc.Server) {
		proto.RegisterDockerServer(s, NewDocker())
	})
}

// Docker implements the proto server
type Docker struct {
}

var _ proto.DockerServer = &Docker{}

// NewDocker returns a new projects server
func NewDocker() *Docker {
	return &Docker{}
}

func (d *Docker) Inspect(ctx context.Context, req *proto.DockerInspectRequest) (*proto.DockerInspectResponse, error) {
	id, err := parseImage(req.Image)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "image not valid")
	}

	auth := &docker.Auth{}
	if req.Registry != "" {
		config, err := k8s.SecretsRegistryGet(req.ProjectId, req.Registry)
		if err != nil {
			log.Println(err)
			return nil, status.Error(codes.NotFound, fmt.Sprintf("%s not found", req.Registry))
		}

		auth.Username = config.Username
		auth.Password = config.Password
	}

	config, err := docker.DownloadManifestConfig("https://"+id.Registry, id.Namespace+"/"+id.Name, id.Tag, auth)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	resp := &proto.DockerInspectResponse{}
	for _, env := range config.Config.Env {
		p := strings.SplitN(env, "=", 2)
		switch p[0] {
		case "PATH":
			continue
		}
		resp.Env = append(resp.Env, &proto.DockerEnv{
			Name:  p[0],
			Value: p[1],
		})
	}

	for prt := range config.Config.ExposedPorts {
		resp.Ports = append(resp.Ports, parsePort(prt))
	}

	return resp, nil
}

type ImageDetails struct {
	Name      string
	Namespace string
	Registry  string
	Tag       string
}

func parseImage(img string) (id ImageDetails, err error) {
	img = strings.TrimSpace(img)
	if img == "" {
		return id, fmt.Errorf("path empty")
	}

	tagSplit := strings.SplitN(img, ":", 2)
	rest := tagSplit[0]
	if len(tagSplit) == 2 {
		id.Tag = tagSplit[1]
	} else {
		id.Tag = "latest"
	}

	pathSplit := strings.SplitN(rest, "/", 3)
	switch len(pathSplit) {
	case 3:
		id.Registry = pathSplit[0]
		id.Namespace = pathSplit[1]
		id.Name = pathSplit[2]
	case 2:
		id.Registry = "registry-1.docker.io"
		id.Namespace = pathSplit[0]
		id.Name = pathSplit[1]
	case 1:
		id.Registry = "registry-1.docker.io"
		id.Namespace = "library"
		id.Name = pathSplit[0]
	default:
		panic("Unreachable")
	}

	return id, nil
}

func parsePort(port string) *proto.DockerPort {
	parts := strings.SplitN(port, "/", 2)

	dp := &proto.DockerPort{
		Port: parts[0],
		Type: proto.DockerPort_tcp,
	}

	if len(parts) == 2 {
		switch parts[0] {
		case "tcp":
			dp.Type = proto.DockerPort_tcp
		case "udp":
			dp.Type = proto.DockerPort_udp
			// default:
		}
	}
	return dp
}
