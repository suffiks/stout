import Service from "@ember/service";
import { computed, on, observes } from "@ember-decorators/object";
import fetch from "fetch";

export default class AuthManagerService extends Service {
  accessToken = "";
  currentProject = null;

  @on("init")
  initialize() {
    let accessToken = localStorage.getItem("accessToken");
    if (accessToken) this.set("accessToken", accessToken);
  }

  @observes("accessToken")
  saveToken({ accessToken }) {
    let stored = localStorage.getItem("accessToken");
    if (stored != accessToken) {
      if (accessToken) {
        localStorage.setItem("accessToken", accessToken);
      } else {
        localStorage.removeItem("accessToken");
      }
    }
  }

  authenticate(email, password) {
    return fetch("/v1/auth/login", {
      method: "POST",
      body: JSON.stringify({ email, password }),
    })
      .then(response => response.json())
      .then(response => {
        if (response.token) {
          this.set("accessToken", response.token);
          return Promise.resolve(true);
        }
        return Promise.reject("missing token");
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }

  invalidate() {
    this.set("accessToken", null);
  }

  @computed("accessToken")
  get loggedIn() {
    return !!this.get("accessToken");
  }

  @computed("accessToken")
  get currentUser() {
    const accessToken = this.get("accessToken");
    var base64Url = accessToken.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(atob(base64));
  }
}
