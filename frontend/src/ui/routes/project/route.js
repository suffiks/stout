import Route from "@ember/routing/route";
import { inject as service } from "@ember-decorators/service";

export default class ProjectsSingleRoute extends Route {
  @service authManager;

  model({ project_id }) {
    var record = this.store.findRecord("project", project_id, { reload: true });

    this.set("authManager.currentProject", record);

    return record;
  }
}
