import Route from "@ember/routing/route";

export default class AppsSingleRoute extends Route {
	model({ app_id }) {
		const project = this.modelFor("project");
		return this.store.findRecord("app", app_id, {
			reload: true,
			adapterOptions: {
				projectID: project.get("id"),
			},
		});
	}
}
