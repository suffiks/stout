import Component, { tracked } from "sparkles-component";
import { Policies } from "stout/src/data/models/auto-deployment/model";
import { observes } from "@ember-decorators/object";

export default class AutoDeploymentComponent extends Component {
  policies = Policies;
  @tracked currentPolicy = "off";

  @tracked("currentPolicy")
  get policy() {
    const policyID = this.args.model.get("policy");
    if (!policyID) {
      this.args.model.set("policy", this.currentPolicy);
    } else {
      this.currentPolicy = policyID;
    }
    return this.currentPolicy;
  }

  @tracked("currentPolicy")
  get requireData() {
    return this.policies.find(v => v.id == this.currentPolicy).data;
  }

  @observes("currentPolicy")
  hello() {
    this.args.model.set("policy", this.currentPolicy);
  }
}
