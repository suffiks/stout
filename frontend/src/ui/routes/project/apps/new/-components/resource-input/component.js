import Component from "@ember/component";
import { on, observes } from "@ember-decorators/object";
// import { computed } from '@ember-decorators/object';

const converters = {
	millicpu(v) {
		return v;
	},
	cpu(v) {
		return v * 1000;
	},
	kB(v) {
		return v * 1000;
	},
	MB(v) {
		return v * 1000 * 1000;
	},
	GB(v) {
		return v * 1000 * 1000 * 1000;
	},
};

export default class InputComponent extends Component {
	type = "bytes";

	typeValue;
	types = {
		cpu: ["millicpu", "cpu"],
		bytes: ["kB", "MB", "GB"],
	};

	@on("didInsertElement")
	setTypeValue() {
		if (this.get("type") == "cpu") {
			this.set("typeValue", "millicpu");
			this.set("inputValue", 250);
			return;
		}

		this.set("typeValue", "MB");
		this.set("inputValue", 128);
	}

	@observes("inputValue", "typeValue")
	setValue() {
		const { inputValue, typeValue } = this.getProperties("inputValue", "typeValue");
		const convert = converters[typeValue];
		if (!convert) return;
		this.set("value", convert(parseInt(inputValue)));
	}
}
