import Route from "@ember/routing/route";
import { action } from "@ember-decorators/object";

export default class AppsNewRoute extends Route {
  model() {
    const project = this.modelFor("project");
    return project.apps.createRecord({
      autoDeployment: this.store.createRecord("auto-deployment"),
    });
  }

  @action
  willTransition() {
    const model = this.get("controller.model");
    if (model.get("isNew")) {
      model.deleteRecord();
    }
  }
}
