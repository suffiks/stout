import Controller from "@ember/controller";
import { action, computed, observes } from "@ember-decorators/object";
import { inject as service } from "@ember-decorators/service";
import fetch, { AbortController } from "fetch";

const inspectionTimeout = 800;

export default class AppsNewController extends Controller {
  inspected = "";
  inspectionTimeout = null;
  inspecting = null;
  automaticID = true;

  registries = [];

  @service authManager;

  @action
  save() {
    this.get("model")
      .save()
      .then(() => {
        this.transitionToRoute("projects.apps.show", this.get("model.project.id"));
      });
  }

  @action
  addPort() {
    const port = this.store.createRecord("port", { name: "hello", port: 80 });
    console.log("ADD", port);
    this.get("model.ports").pushObject(port);
  }

  @observes("model.project.id")
  loadRegistries() {
    this.set("registries", []);
    this.get("model.project.registries").then(registries => {
      this.set("registries", registries);
    });
  }

  //
  // Generate ID
  //
  @action
  idChange() {
    this.automaticID = false;
  }

  @observes("model.name")
  createID() {
    const model = this.get("model");
    const name = model.get("name");
    if (typeof name != "string") return;

    model.set(
      "id",
      name
        .toLowerCase()
        .replace(/\s|\\-/g, "_")
        .replace(/[^a-z0-9_]|^[^a-z]+|[^a-z]+$/g, "")
        .replace(/_+/g, "_")
    );
  }

  //
  // Inspect images
  //
  @observes("model.registry", "model.image")
  inspectImageObserver() {
    const model = this.get("model");
    const { image, registry } = model.getProperties("image", "registry");
    this.abort();

    if (!image) {
      return;
    }
    if (this.get("inspected") == `${image}-${registry}`) return;

    this.set(
      "inspectionTimeout",
      setTimeout(() => {
        this.inspectImage(image, registry);
      }, inspectionTimeout)
    );
  }

  @computed("inspecting")
  get disabled() {
    return !!this.get("inspecting");
  }

  inspectImage(image, registry) {
    this.abort();
    let controller = new AbortController();

    const headers = {
      Authorization: `Bearer ${this.get("authManager.accessToken")}`,
    };

    let url = `/v1/docker?image=${encodeURIComponent(image)}`;
    if (registry) url += `&registry=${encodeURIComponent(registry)}&project_id=${encodeURIComponent(this.get("model.project.id"))}`;

    this.set("inspecting", controller);
    fetch(url, {
      headers,
      signal: controller.signal,
    })
      .then(response => response.json())
      .then(data => {
        const model = this.get("model");
        if (data.ports)
          data.ports.forEach(port => {
            model.get("ports").pushObject(this.store.createRecord("port", port));
          });
        if (data.env)
          data.env.forEach(env => {
            model.get("env").pushObject(this.store.createRecord("envvar", env));
          });
      })
      .catch(() => {
        console.log("Error inspecting image", image);
      })
      .finally(() => {
        this.set("inspecting", null);
      });
  }

  abort(justClean = false) {
    const it = this.get("inspectionTimeout");
    if (it) {
      clearTimeout(it);
    }

    let abortController = this.get("inspecting");
    if (abortController) {
      if (!justClean) {
        console.log("Aborted");
        abortController.abort();
      }
      this.set("inspecting", null);
    }
  }
}
