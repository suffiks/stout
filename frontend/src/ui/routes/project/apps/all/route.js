import Route from "@ember/routing/route";

export default class AppsAllRoute extends Route {
	model() {
		return this.modelFor("project");
	}
}
