import Route from "@ember/routing/route";

export default class UsersAllRoute extends Route {
	model() {
		const project = this.modelFor("project");
		return project.get("users");
	}
}
