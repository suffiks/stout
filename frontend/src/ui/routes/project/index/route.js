import Route from "@ember/routing/route";

export default class ProjectsSingleIndexRoute extends Route {
	model() {
		return this.modelFor("project");
	}
}
