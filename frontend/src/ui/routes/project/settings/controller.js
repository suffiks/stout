import Controller from "@ember/controller";
import { action } from "@ember-decorators/object";

export default class ProjectSettingsController extends Controller {
  @action
  deleteProject() {
    if (!confirm("Are you sure you want to delete the project?")) return;

    this.get("model")
      .destroyRecord()
      .then(() => {
        this.transitionToRoute("projects");
      });
  }
}
