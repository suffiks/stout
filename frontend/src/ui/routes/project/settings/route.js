import Route from "@ember/routing/route";

export default class ProjectsSingleSettingsRoute extends Route {
	model() {
		return this.modelFor("project");
	}
}
