import Controller from "@ember/controller";
import { action /*, computed, on, observes */ } from "@ember-decorators/object";
// import { reads } from '@ember-decorators/object/computed';
// import { inject as service } from '@ember-decorators/service';

export default class RegistriesNewController extends Controller {
  @action
  save() {
    this.get("model")
      .save()
      .then(() => {
        this.transitionToRoute("project.registries", this.get("model.project.id"));
      });
  }
}
