import Route from "@ember/routing/route";
import { action } from "@ember-decorators/object";

export default class RegistriesNewRoute extends Route {
	model() {
		const project = this.modelFor("project");
		return project.registries.createRecord("registry");
	}

	@action
	willTransition() {
		const model = this.get("controller.model");
		if (model.get("isNew")) {
			model.deleteRecord();
		}
	}
}
