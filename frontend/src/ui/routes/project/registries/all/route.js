import Route from "@ember/routing/route";

export default class RegistriesAllRoute extends Route {
	model() {
		const project = this.modelFor("project");
		return project.get("registries");
	}
}
