import Controller from "@ember/controller";
import { action, computed, on, observes } from "@ember-decorators/object";
import { reads } from "@ember-decorators/object/computed";
import { inject as service } from "@ember-decorators/service";

export default class Application extends Controller {
  @service authManager;

  @reads("authManager.currentUser") currentUser;
  @reads("authManager.currentProject") currentProject;

  @on("init")
  @observes("loggedIn")
  loadProjects() {
    if (this.get("loggedIn")) {
      this.store.findAll("project").catch(() => {
        return [];
      });
    } else {
      this.store.unloadAll();
    }
  }

  @computed("authManager.loggedIn")
  get loggedIn() {
    return this.get("authManager.loggedIn");
  }

  @action
  logout() {
    this.get("authManager").invalidate();
    this.transitionToRoute("login").then(() => {
      window.location.reload();
    });
  }
}
