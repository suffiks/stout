import Route from "@ember/routing/route";
import DS from "ember-data";
import { action } from "@ember-decorators/object";
import { inject as service } from "@ember-decorators/service";

export default class ApplicationRoute extends Route {
  @service authManager;

  beforeModel(transition) {
    const loggedIn = this.get("authManager.loggedIn");
    if (!loggedIn) {
      const target = transition.targetName;
      switch (target) {
        case "login":
        case "register":
          break;
        default:
          this.transitionTo("login");
          break;
      }
    }

    return super.beforeModel(...arguments);
  }

  model() {
    return this.store.peekAll("project");
  }

  @action
  error(errors) {
    console.log("errors", errors);
    if (errors instanceof DS.UnauthorizedError) {
      console.log("Invalidate", errors);
      this.authManager.invalidate();
      if (window) window.location.reload();
    }
  }
}
