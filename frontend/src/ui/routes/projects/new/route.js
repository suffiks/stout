import Route from "@ember/routing/route";

export default class ProjectsNewRoute extends Route {
	model() {
		return this.store.createRecord("project");
	}
}
