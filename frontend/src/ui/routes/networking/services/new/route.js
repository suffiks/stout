import Route from "@ember/routing/route";
import { action } from "@ember-decorators/object";

export default class ServicesNewRoute extends Route {
  model() {
    const project = this.modelFor("project");
    return this.store.createRecord("service", {
      project,
    });
  }

  @action
  willTransition() {
    const model = this.get("controller.model");
    if (model.get("isNew")) {
      model.deleteRecord();
    }
  }
}
