import Controller from "@ember/controller";
import { action, computed, observes } from "@ember-decorators/object";

export default class ServicesNewController extends Controller {
  ports = [];

  @computed("model.project.id")
  get apps() {
    return this.get("model.project.apps");
  }

  @observes("model.app")
  updatePorts() {
    console.log("Update ports");
    this.set("ports", [
      {
        id: "web",
        name: "web (8080)",
      },
    ]);
    this.set("model.portName", "");
    // this.store.findRecord("app", appID, {
    //   reload: true,
    //   adapterOptions: {
    //     projectID: this.get("model.project.id"),
    //   },
    // });
  }

  @computed()
  get appID() {
    return this.get("model.app.id");
  }

  set appID(val) {
    this.set("model.app", this.get("apps").find(v => v.get("id") == val));
  }

  @action
  save() {
    this.set("model.type", 0);

    this.get("model")
      .save()
      .then(() => {
        this.transitionToRoute("networking.services", this.get("model.project"));
      });
  }
}
