import Route from "@ember/routing/route";
import { inject as service } from "@ember-decorators/service";

export default class LoginRoute extends Route {
  @service authManager;
  beforeModel() {
    if (this.get("authManager.loggedIn")) {
      this.transitionTo("");
    }
  }
}
