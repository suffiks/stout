import Controller from "@ember/controller";
import { action } from "@ember-decorators/object";
import { inject as service } from "@ember-decorators/service";

export default class LoginRoute extends Controller {
  email = "";
  password = "";

  @service authManager;

  @action
  submit() {
    let { email, password } = this.getProperties("email", "password");

    this.get("authManager")
      .authenticate(email, password)
      .then(() => {
        this.transitionToRoute("");
      })
      .catch(err => {
        console.log(err);
        alert("error");
      });
  }
}
