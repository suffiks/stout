import Component, { tracked } from "sparkles-component";

var ddnum = 0;

export default class DropDownComponent extends Component {
  elID = `ddid-${++ddnum}`;
  el = null;
  @tracked
  visible = false;

  didInsertElement() {
    // https://github.com/cibernox/sparkles-ember-basic-dropdown/blob/master/addon/components/basic-dropdown-trigger.ts
    this.el = document.getElementById(this.elID);
    this._addHandlers();
  }

  _addHandlers() {
    if (!this.el) return;

    this.el.addEventListener("mouseenter", () => this.mouseEnter());
    this.el.addEventListener("mouseleave", () => this.mouseLeave());
  }

  mouseEnter() {
    this.visible = true;
  }

  mouseLeave() {
    this.visible = false;
  }
}
