import Component from "@ember/component";
import { action, computed } from "@ember-decorators/object";
import { get } from "@ember/object";

var increments = 0;
export default class SelectComponent extends Component {
  classNames = ["form-group"];
  id = `siid-${++increments}`;

  value = "";
  data = [];
  key = "id";
  valueKey = "name";
  searchEnabled = false;
  allowClear = true;
  disabled = false;
  force = false;

  @action
  select(selection) {
    if (!selection) {
      this.set("value", null);
      return;
    }
    if (selection.get) {
      this.set("value", selection.get(this.get("key")));
      return;
    }

    if (this.get("force")) {
      this.set("value", get(selection, this.get("key")));
      return;
    }

    this.set("value", selection);
  }

  @computed("value", "data.[]", "key")
  get selection() {
    const { value, data, key, force } = this.getProperties("value", "data", "key", "force");
    return data.find(d => {
      if (d.get) return d.get(key) == value;
      if (force) return get(d, key) == value;
      return d == value;
    });
  }

  @computed("value")
  get inputClasses() {
    if (this.get("value.length") > 0) {
      return "has-content";
    }
    return "";
  }
}
