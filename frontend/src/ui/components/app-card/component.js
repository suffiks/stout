import Component, { tracked } from "sparkles-component";

import { reads } from "@ember-decorators/object/computed";
// import { on } from '@ember-decorators/object';
export default class AppCardComponent extends Component {
  @tracked @reads("args.app.status") status;

  @tracked("status")
  get statusColor() {
    const status = this.status;

    if (status == "ok") {
      return "green";
    }
    return "yellow";
  }

  @tracked("args", "size")
  get size() {
    const { size } = this.args;

    return size || 4;
  }
}
