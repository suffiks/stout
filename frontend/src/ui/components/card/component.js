import Component, { tracked } from "sparkles-component";

export default class CardComponent extends Component {
  @tracked("args")
  get sizeClass() {
    return `size-${this.args.size}`;
  }
}
