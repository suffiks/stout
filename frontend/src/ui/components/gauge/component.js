import Component from "@ember/component";
import { computed } from "@ember-decorators/object";

export default class GaugeComponent extends Component {
	tagName = "svg";
	classNames = ["gauge"];
	attributeBindings = ["viewBox"];

	max = 100;
	value = 20;
	color = "blue";
	label = "";

	get viewBox() {
		return "0 0 1000 500";
	}

	@computed("value", "max")
	get percentLabel() {
		let { value, max } = this.getProperties("value", "max");
		if (value == undefined) value = 0;
		var ratio = ((1.0 * value) / max) * 100;
		return `${ratio.toFixed(2).replace(/[.,]00$/, "")}%`;
	}

	@computed("value", "max")
	get path() {
		let { value, max } = this.getProperties("value", "max");
		if (value == undefined) value = 0;
		value = parseFloat(value);
		max = parseFloat(max);
		if (value > max) {
			value = max;
		}

		const ratio = value / max;
		return createArc(ratio);
	}
}

var createArc = function(ratio) {
	return svg_arc_path(500, 500, 450, -90, ratio * 180.0 - 90);
};

var polar_to_cartesian = function(cx, cy, radius, angle) {
	var radians;
	radians = ((angle - 90) * Math.PI) / 180.0;
	return [Math.round((cx + radius * Math.cos(radians)) * 100) / 100, Math.round((cy + radius * Math.sin(radians)) * 100) / 100];
};

var svg_arc_path = function(x, y, radius, start_angle, end_angle) {
	var end_xy, start_xy;
	start_xy = polar_to_cartesian(x, y, radius, end_angle);
	end_xy = polar_to_cartesian(x, y, radius, start_angle);
	return "M " + start_xy[0] + " " + start_xy[1] + " A " + radius + " " + radius + " 0 0 0 " + end_xy[0] + " " + end_xy[1];
};
