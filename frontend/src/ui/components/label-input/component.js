import Component from "@ember/component";
import { computed } from "@ember-decorators/object";

var increments = 1;

export default class InputComponent extends Component {
	classNames = ["form-group"];
	id = `liid-${++increments}`;

	disabled = false;

	@computed("value")
	get inputClasses() {
		if (this.get("value.length") > 0 || this.get("value") > 0) {
			return "has-content";
		}
		return "";
	}
}
