import EmberRouter from "@ember/routing/router";
import config from "../config/environment";

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL,
});

Router.map(function() {
  this.route("login");
  this.route("projects", { path: "/" }, function() {
    this.route("all", { path: "/" });
    this.route("new");

    this.route("project", { path: "/:project_id", resetNamespace: true }, function() {
      this.route("index", { path: "/" });
      this.route("settings");

      this.route("apps", function() {
        this.route("all", { path: "/" });
        this.route("show", { path: ":app_id" });
        this.route("new");
      });

      this.route("registries", function() {
        this.route("all", { path: "/" });
        this.route("new");
      });

      this.route("networking", { resetNamespace: true }, function() {
        this.route("services", { path: "/" }, function() {
          this.route("new");
        });

        this.route("dns", { path: "/dns" });
      });

      this.route("users", function() {
        this.route("all", { path: "/" });
      });
    });
  });
});

export default Router;
