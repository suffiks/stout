import DS from "ember-data";
import { attr } from "@ember-decorators/data";

const { Model } = DS;

export default class LimitsModel extends Model {
	@attr("number")
	cpu;

	@attr("number")
	memory;

	@attr("number")
	storage;

	@attr("number")
	storage_claims;
}
