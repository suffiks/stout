import DS from "ember-data";
import { attr } from "@ember-decorators/data";

const { Model } = DS;

const Policies = [
  {
    id: "off",
    name: "Off",
  },
  {
    id: "all_semver",
    name: "All semver",
  },
  {
    id: "major",
    name: "Major",
  },
  {
    id: "minor",
    name: "Minor",
  },
  {
    id: "patch",
    name: "Patch",
  },
  {
    id: "tags_all",
    name: "All tags",
  },
  {
    id: "tags_match",
    name: "Matching tags",
  },
  {
    id: "glob",
    name: "Glob",
    data: true,
  },
];

export { Policies };

export default class AutoDeploymentModel extends Model {
  @attr("string", { default: "off" })
  policy;

  @attr("string")
  policy_data;

  @attr("boolean")
  require_approval;

  @attr("boolean")
  pull;
}
