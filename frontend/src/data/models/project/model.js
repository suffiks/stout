import DS from "ember-data";
import { attr, hasMany } from "@ember-decorators/data";

const { Model } = DS;

export default class ProjectModel extends Model {
  @attr("string")
  name;

  @attr("object")
  limits;

  @attr("object")
  used;

  @hasMany("app", { async: true })
  apps;

  @hasMany("registry", { async: true })
  registries;

  @hasMany("users", { async: true })
  users;

  @hasMany("service", { async: true })
  services;
}
