import ApplicationSerializer from "../application/serializer";

export default class ServiceSerializer extends ApplicationSerializer {
  attrs = {
    app: "app_id",
  };
}
