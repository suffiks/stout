import DS from "ember-data";
import { attr, belongsTo } from "@ember-decorators/data";

const { Model } = DS;

export default class ServiceModel extends Model {
  @attr("string")
  name;

  @attr("number")
  type;

  @attr("string")
  portName;

  @attr("number")
  expose;

  @belongsTo("project")
  project;

  @belongsTo("app")
  app;
}
