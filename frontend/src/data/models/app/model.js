import DS from "ember-data";
import { attr, belongsTo, hasMany } from "@ember-decorators/data";

const { Model } = DS;

export default class AppModel extends Model {
  @attr("string")
  name;

  @attr("string")
  status;

  @attr("string")
  image;

  @attr("string")
  registry;

  @attr("number")
  cpu;

  @attr("number")
  memory;

  @attr("number", { defaultValue: 1 })
  replicas;

  @belongsTo("project")
  project;

  @hasMany("port")
  ports;

  @hasMany("envvar")
  env;

  @belongsTo("auto-deployment")
  autoDeployment;
}
