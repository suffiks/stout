import DS from "ember-data";
import ApplicationSerializer from "../application/serializer";

export default class AppSerializer extends ApplicationSerializer.extend(DS.EmbeddedRecordsMixin) {
  attrs = {
    status: { serialize: false },
    ports: { embedded: "always" },
    env: { embedded: "always" },
    autoDeployment: { embedded: "always" },
  };
}
