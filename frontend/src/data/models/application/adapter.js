import DS from "ember-data";
import { assign } from "@ember/polyfills";
import { inject as service } from "@ember-decorators/service";
import { get } from "@ember/object";
import RSVP from "rsvp";
import AdapterFetch from "ember-fetch/mixins/adapter-fetch";

const { RESTAdapter } = DS;

class ApplicationAdapter extends RESTAdapter.extend(AdapterFetch) {
  namespace = "v1";

  @service authManager;

  ajaxOptions() {
    let hash = super.ajaxOptions(...arguments);

    const accessToken = this.get("authManager.accessToken");
    if (accessToken) {
      hash.headers = assign(hash.headers || {}, { Authorization: `Bearer ${accessToken}` });
    }

    return hash;
  }

  async findHasMany(nu1, nu2, nu3, relationship) {
    const d = await super.findHasMany(...arguments);
    if (!d || Object.keys(d).length == 0) {
      let data = {};
      data[relationship.meta.key] = [];
      return RSVP.resolve(data);
    }
    return RSVP.resolve(d);
  }
}

export class SubProjectAdapter extends ApplicationAdapter {
  buildProjectURL(modelName, id, snapshot) {
    let url = [];

    const host = this.get("host");
    if (host) url.push(host);

    const prefix = this.urlPrefix();
    if (prefix) url.push(prefix);

    let projectID = get(snapshot, "adapterOptions.projectID");
    if (!projectID) projectID = snapshot.belongsTo("project", { id: true });
    if (projectID) url.push("projects", projectID);

    if (modelName) url.push(this.pathForType(modelName));

    if (id) url.push(encodeURIComponent(id));

    let ret = url.join("/");
    if (!host && ret.charAt(0) !== "/") ret = "/" + ret;

    return ret;
  }

  urlForFindRecord(id, modelName, snapshot) {
    return this.buildProjectURL(modelName, id, snapshot);
  }

  urlForFindAll(modelName, snapshot) {
    return this.buildProjectURL(modelName, null, snapshot);
  }

  urlForCreateRecord(modelName, snapshot) {
    return this.buildProjectURL(modelName, null, snapshot);
  }

  urlForUpdateRecord(id, modelName, snapshot) {
    return this.buildProjectURL(modelName, id, snapshot);
  }
}

export default ApplicationAdapter;
