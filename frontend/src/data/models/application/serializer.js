import DS from "ember-data";
import { get } from "@ember/object";
import { assign } from "@ember/polyfills";

const { RESTSerializer } = DS;

export default class ApplicationSerializer extends RESTSerializer {
	normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
		const data = {};
		data[primaryModelClass.modelName] = payload;
		return super.normalizeSingleResponse(store, primaryModelClass, data, id, requestType);
	}

	normalize(typeClass) {
		let jsonapi = super.normalize(...arguments);
		const relationships = get(typeClass, "relationships");

		relationships.forEach(val => {
			const key = val[0].meta.name;

			if (val[0].meta.options.async) {
				if (!jsonapi.data.relationships[key]) {
					jsonapi.data.relationships[key] = {
						links: { related: key },
					};
				}
			}
		});

		return jsonapi;
	}

	serializeIntoHash(hash, nu, snapshot, options) {
		assign(hash, this.serialize(snapshot, options));
	}
}
