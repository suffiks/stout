import DS from "ember-data";
import { attr } from "@ember-decorators/data";

const { Model } = DS;

export default class PortModel extends Model {
  @attr("string", { default: "web" })
  name;

  @attr("number", { default: 80 })
  port;
}
