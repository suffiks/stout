import DS from "ember-data";
import { attr, belongsTo } from "@ember-decorators/data";

const { Model } = DS;

export default class RegistryModel extends Model {
	@attr("string")
	name;

	@attr("string")
	email;

	@attr("string", { defaultValue: "https://index.docker.io/v1/" })
	server;

	@attr("string")
	username;

	@attr("string")
	password;

	@belongsTo
	project;
}
