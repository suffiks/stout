import DS from "ember-data";
import { attr, belongsTo } from "@ember-decorators/data";

const { Model } = DS;

export default class UserModel extends Model {
	@attr("string")
	name;

	@belongsTo
	project;
}
