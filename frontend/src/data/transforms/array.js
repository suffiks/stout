import DS from "ember-data";
const { Transform } = DS;

export default class ArrayTransform extends Transform {
	serialize(deserialized) {
		return deserialized;
	}

	deserialize(serialized) {
		if (Array.isArray(serialized)) return serialized;
		return JSON.parse(serialized);
	}
}
